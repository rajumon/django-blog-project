from django.db import models

# Create your models here.


class singleBlog(models.Model):
    title = models.CharField(max_length=255, blank=False)
    description = models.TextField(max_length=1500, blank=False)
    image = models.ImageField(upload_to=(
        'blog_post/'), blank=False, null=False)

    def __str__(self):
        return self.title
