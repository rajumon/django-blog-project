from django.contrib import admin

from .models import About
from .models import Slider

admin.site.register(About)
admin.site.register(Slider)
