from django.shortcuts import render
from .models import About
from .models import Slider


def home(request):
    dataget = About.objects.all().last()
    sliderdata = Slider.objects.all()
    contex = {
        'aboutus': dataget,
        'slider': sliderdata
    }
    return render(request, 'index.html', contex)


def about(request):
    return render(request, 'about.html')
