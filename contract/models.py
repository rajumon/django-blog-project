from django.db import models


class Contract(models.Model):
    name = models.CharField(max_length=100, blank=False)
    email = models.TextField(max_length=100, blank=False)
    subject = models.TextField(max_length=150, blank=False)
    massege = models.TextField(max_length=700, blank=False)

    def __str__(self):
        return self.subject
