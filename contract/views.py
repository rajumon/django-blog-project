from django.shortcuts import render
from .models import Contract


def contract(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')
        contractdata = Contract(name=name, email=email,
                                subject=subject, massege=message)
        contractdata.save()
    return render(request, 'contract.html')
